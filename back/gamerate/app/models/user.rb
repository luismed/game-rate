class User < ApplicationRecord

    validates :name, presence: true
    validates :name, uniqueness: true
    validates :name, length: { minimum: 8 }
    validates :email, presence: true
    validates :email, uniqueness: true
    validates :password_digest, presence: true
    validates :admin, presence: true
    validates :developer, presence: true

    #def checkDevoleper 
    # if developer
            
    #   end
    #end

end
